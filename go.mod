module gitlab.com/cyverse/cacao-common

go 1.14

require (
	github.com/cloudevents/sdk-go/v2 v2.3.1
	github.com/go-git/go-billy/v5 v5.3.1
	github.com/go-git/go-git/v5 v5.3.0
	github.com/nats-io/nats-streaming-server v0.19.0 // indirect
	github.com/nats-io/nats.go v1.10.0
	github.com/nats-io/stan.go v0.8.2
	github.com/rs/xid v1.2.1
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.6.1
	go.mongodb.org/mongo-driver v1.5.2
)
