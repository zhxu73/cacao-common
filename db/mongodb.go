// Package db contains utility functions for dealing with database
package db

import (
	"context"
	"errors"
	"fmt"

	log "github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	// DefaultMongoDBURL is a default mongodb URL
	DefaultMongoDBURL = "mongodb://localhost:27017"
	// DefaultMongoDBName is a default db name
	DefaultMongoDBName = "cacao"
)

// MongoDBConfig stores configurations used by mongodb
type MongoDBConfig struct {
	URL    string `envconfig:"MONGODB_URL" default:"mongodb://localhost:27017"`
	DBName string `envconfig:"MONGODB_DB_NAME" default:"cacao"`
}

// MongoDBConnection contains MongoDB connection info
type MongoDBConnection struct {
	Config   *MongoDBConfig
	Client   *mongo.Client
	Database *mongo.Database
}

// NewMongoDBConnection connects to MongoDB
func NewMongoDBConnection(config *MongoDBConfig) (*MongoDBConnection, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "ConnectMongoDB",
	})

	logger.Tracef("trying to connect to %s", config.URL)

	clientOptions := options.Client().ApplyURI(config.URL)

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		logger.WithError(err).Errorf("failed to connect to %s", config.URL)
		return nil, err
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		logger.WithError(err).Errorf("failed to check the connection to %s", config.URL)
		return nil, err
	}

	database := client.Database(config.DBName)

	logger.Tracef("established a connection to %s", config.URL)

	return &MongoDBConnection{
		Config:   config,
		Client:   client,
		Database: database,
	}, nil
}

// Disconnect disconnects MongoDB connection
func (conn *MongoDBConnection) Disconnect() error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.Disconnect",
	})

	logger.Tracef("trying to disconnect from %s", conn.Config.URL)

	if conn.Client == nil {
		err := fmt.Errorf("client is not connected")
		logger.Error(err)
		return err
	}

	err := conn.Client.Disconnect(context.TODO())
	if err != nil {
		logger.WithError(err).Errorf("failed to disconnect from %s", conn.Config.URL)
		return err
	}

	logger.Tracef("disconnected from %s", conn.Config.URL)

	conn.Client = nil
	conn.Database = nil
	return nil
}

// List lists filtered documents in the given collection
// results must be a pointer to a struct array (i.e., *[]ExampleStruct)
func (conn *MongoDBConnection) List(collection string, filter map[string]interface{}, results interface{}, opts ...*options.FindOptions) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.List",
	})

	logger.Tracef("trying to list documents in a collection %s with a filter %v", collection, filter)

	// get collection
	coll := conn.Database.Collection(collection)

	// find documents
	// Passing bson.M{{}} as the filter
	cursor, err := coll.Find(context.TODO(), filter, opts...)
	if err != nil {
		logger.WithError(err).Errorf("failed to find documents in a collection %s", collection)
		return err
	}
	defer cursor.Close(context.TODO())

	err = cursor.All(context.TODO(), results)
	if err != nil {
		logger.WithError(err).Error("failed to read documents from a cursor")
		return err
	}

	logger.Tracef("listed documents in a collection %s", collection)

	return nil
}

// ListAndGetCursor lists filtered documents in the given collection, and returns a cursor
func (conn *MongoDBConnection) ListAndGetCursor(collection string, filter map[string]interface{}, opts ...*options.FindOptions) (*mongo.Cursor, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.ListAndGetCursor",
	})

	logger.Tracef("trying to list documents in a collection %s with a filter %v", collection, filter)

	// get collection
	coll := conn.Database.Collection(collection)

	// find documents
	// Passing bson.M{{}} as the filter
	cursor, err := coll.Find(context.TODO(), filter, opts...)
	if err != nil {
		logger.WithError(err).Errorf("failed to find documents in a collection %s", collection)
		return nil, err
	}

	logger.Tracef("listed documents in a collection %s", collection)

	return cursor, nil
}

// Get returns a document in the given collection
// results must be a pointer to a struct (i.e., *ExampleStruct)
func (conn *MongoDBConnection) Get(collection string, filter map[string]interface{}, result interface{}, opts ...*options.FindOneOptions) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.Get",
	})

	logger.Tracef("trying to get a document in a collection %s with a filter %v", collection, filter)

	// get collection
	coll := conn.Database.Collection(collection)

	// find a document
	// Passing bson.M{{}} as the filter
	singleResult := coll.FindOne(context.TODO(), filter, opts...)

	if singleResult.Err() != nil {
		err := singleResult.Err()
		logger.WithError(err).Errorf("failed to find a document in a collection %s", collection)
		return err
	}

	err := singleResult.Decode(result)
	if err != nil {
		logger.WithError(err).Error("failed to decode a document from a result")
		return err
	}

	logger.Tracef("got a document in a collection %s", collection)

	return nil
}

// Insert inserts a document into the given collection
func (conn *MongoDBConnection) Insert(collection string, document interface{}, opts ...*options.InsertOneOptions) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.Insert",
	})

	logger.Tracef("trying to insert a document into a collection %s", collection)

	// get collection
	coll := conn.Database.Collection(collection)

	// insert a document
	_, err := coll.InsertOne(context.TODO(), document, opts...)
	if err != nil {
		logger.WithError(err).Errorf("failed to insert a document into a collection %s", collection)
		return err
	}

	logger.Tracef("inserted a document into a collection %s", collection)

	return nil
}

// InsertMany inserts multiple documents into the given collection
func (conn *MongoDBConnection) InsertMany(collection string, documents []interface{}, opts ...*options.InsertManyOptions) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.InsertMany",
	})

	logger.Tracef("trying to insert %d documents into a collection %s", len(documents), collection)

	// get collection
	coll := conn.Database.Collection(collection)

	// insert documents
	result, err := coll.InsertMany(context.TODO(), documents, opts...)
	if err != nil {
		logger.WithError(err).Errorf("failed to insert documents into a collection %s", collection)
		return err
	}

	logger.Tracef("inserted %d documents into a collection %s", len(result.InsertedIDs), collection)

	return nil
}

// Update updates a document in the given collection, returns true if updated successfully
func (conn *MongoDBConnection) Update(collection string, filter map[string]interface{}, updateValues map[string]interface{}, opts ...*options.UpdateOptions) (bool, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.Update",
	})

	logger.Tracef("trying to update a document in a collection %s with a filter %v", collection, filter)

	// get collection
	coll := conn.Database.Collection(collection)

	// update a document
	result, err := coll.UpdateOne(context.TODO(), filter, updateValues, opts...)
	if err != nil {
		logger.WithError(err).Errorf("failed to update a document in a collection %s", collection)
		return false, err
	}

	if result.MatchedCount == 0 {
		logger.WithError(err).Errorf("failed to find a matching document in a collection %s", collection)
		return false, nil
	}

	logger.Tracef("updated a document in a collection %s", collection)

	return true, nil
}

// UpdateMany updates multiple document in the given collection, returns the number of documents updated as the first result
func (conn *MongoDBConnection) UpdateMany(collection string, filter map[string]interface{}, updateValues map[string]interface{}, opts ...*options.UpdateOptions) (int64, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.UpdateMany",
	})

	logger.Tracef("trying to update documents in a collection %s with a filter %v", collection, filter)

	// get collection
	coll := conn.Database.Collection(collection)

	// update documents
	result, err := coll.UpdateMany(context.TODO(), filter, updateValues, opts...)
	if err != nil {
		logger.WithError(err).Errorf("failed to update documents in a collection %s", collection)
		return 0, err
	}

	logger.Tracef("updated %d documents in a collection %s", result.MatchedCount, collection)

	return result.MatchedCount, nil
}

// Replace replaces a document in the given collection, returns true if replaced successfully
// Replace is different from Update. It updates an entire document, while Update updates some fields in a document.
func (conn *MongoDBConnection) Replace(collection string, filter map[string]interface{}, replaceDocument interface{}, opts ...*options.ReplaceOptions) (bool, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.Replace",
	})

	logger.Tracef("trying to replace a document in a collection %s with a filter %v", collection, filter)

	// get collection
	coll := conn.Database.Collection(collection)

	// replace a document
	result, err := coll.ReplaceOne(context.TODO(), filter, replaceDocument, opts...)
	if err != nil {
		logger.WithError(err).Errorf("failed to replace a document in a collection %s", collection)
		return false, err
	}

	if result.MatchedCount == 0 {
		logger.WithError(err).Errorf("failed to find a matching document in a collection %s", collection)
		return false, nil
	}

	logger.Tracef("replaced a document in a collection %s", collection)

	return true, nil
}

// Delete deletes a document in the given collection, returns true if deleted successfully
func (conn *MongoDBConnection) Delete(collection string, filter map[string]interface{}, opts ...*options.DeleteOptions) (bool, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.Delete",
	})

	logger.Tracef("trying to delete a document in a collection %s with a filter %v", collection, filter)

	// get collection
	coll := conn.Database.Collection(collection)

	// delete a document
	result, err := coll.DeleteOne(context.TODO(), filter, opts...)
	if err != nil {
		logger.WithError(err).Errorf("failed to delete a document in a collection %s", collection)
		return false, err
	}

	if result.DeletedCount == 0 {
		logger.WithError(err).Errorf("failed to find a matching document in a collection %s", collection)
		return false, nil
	}

	logger.Tracef("deleted a document in a collection %s", collection)

	return true, nil
}

// DeleteMany deletes multiple document in the given collection, returns the number of documents deleted as the first result
func (conn *MongoDBConnection) DeleteMany(collection string, filter map[string]interface{}, opts ...*options.DeleteOptions) (int64, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.DeleteMany",
	})

	logger.Tracef("trying to delete documents in a collection %s with a filter %v", collection, filter)

	// get collection
	coll := conn.Database.Collection(collection)

	// delete documents
	result, err := coll.DeleteMany(context.TODO(), filter, opts...)
	if err != nil {
		logger.WithError(err).Errorf("failed to delete documents in a collection %s", collection)
		return 0, err
	}

	logger.Tracef("deleted %d documents in a collection %s", result.DeletedCount, collection)

	return result.DeletedCount, nil
}

// IsDuplicateError returns a true, if the mongodb error received is an duplicate entry on _id
// copied from https://stackoverflow.com/questions/56916969/with-mongodb-go-driver-how-do-i-get-the-inner-exceptions
func IsDuplicateError(err error) bool {
	var e mongo.WriteException
	if errors.As(err, &e) {
		for _, we := range e.WriteErrors {
			if we.Code == 11000 {
				return true
			}
		}
	}
	return false
}
