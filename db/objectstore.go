// Package db contains utility functions for dealing with database
package db

import (
	"reflect"
	"strings"

	log "github.com/sirupsen/logrus"
)

// ObjectStore is an interface for object-level database I/O
type ObjectStore interface {
	Release() error
	List(collection string, results interface{}) error
	ListConditional(collection string, conditions map[string]interface{}, results interface{}) error
	ListForUser(collection string, owner string, results interface{}) error
	Get(collection string, id string, result interface{}) error
	GetConditional(collection string, conditions map[string]interface{}, result interface{}) error
	Insert(collection string, document interface{}) error
	Update(collection string, id string, updateDocument interface{}) (bool, error)
	Replace(collection string, id string, replaceDocument interface{}) (bool, error)
	Delete(collection string, id string) (bool, error)
	DeleteConditional(collection string, conditions map[string]interface{}) (int64, error)
}

// validateDocumentType checks docuement type if it has mandatory fields, such as _id and owner
// The document must contain following bson fields
// _id: the ID of a document
// owner: the owner of a document
func validateDocumentType(document interface{}) bool {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "ObjectStore.validateDocumentType",
	})

	documentType := reflect.TypeOf(document)
	if documentType.Kind() != reflect.Struct {
		return false
	}

	hasIDField := false
	hasOwnerField := false

	for i := 0; i < documentType.NumField(); i++ {
		fieldTag := documentType.Field(i).Tag
		if tagValue, ok := fieldTag.Lookup("bson"); ok {
			tagValues := strings.Split(tagValue, ",")
			bsonFieldName := ""
			if len(tagValues) > 0 {
				bsonFieldName = tagValues[0]
			}

			if bsonFieldName == "_id" {
				// _id exists
				hasIDField = true
			}

			switch bsonFieldName {
			case "_id":
				// _id exists
				hasIDField = true
			case "owner":
				// owner exists
				hasOwnerField = true
			default:
				// do nothing
			}
		}
	}

	if !hasIDField {
		logger.Tracef("cannot find _id field in a docuemnt %s", documentType.Name())
	}

	if !hasOwnerField {
		logger.Tracef("cannot find owner field in a docuemnt %s", documentType.Name())
	}

	return hasIDField && hasOwnerField
}
