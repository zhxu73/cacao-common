// Package messaging contains utility functions for dealing with a massaging service
package messaging

import (
	"fmt"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	stan "github.com/nats-io/stan.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
)

const (
	// DefaultStanEventsSubject is a default subject for cyverse events
	DefaultStanEventsSubject = "cyverse.events"
	// DefaultStanEventsTimeout is a default timeout
	DefaultStanEventsTimeout = 10 * time.Second // used to wait for event ops to complete, is very conservation
	// DefaultStanAckWaitTime is a default wait time for ack wait
	DefaultStanAckWaitTime = 60 * time.Second
)

// StanConfig stores additional configurations used by nats streaming, used along with NatsConfig DefaultNatsReconnectWait
type StanConfig struct {
	ClusterID     string `envconfig:"NATS_CLUSTER_ID" default:"cacao-cluster"`
	DurableName   string `envconfig:"NATS_DURABLE_NAME"`
	EventsTimeout int    `envconfig:"NATS_EVENTS_TIMEOUT" default:"-1"`
}

// StanConnection contains Stan connection info
type StanConnection struct {
	NatsConfig         *NatsConfig
	StanConfig         *StanConfig
	Connection         stan.Conn
	EventHandlers      map[common.EventType]StreamingEventHandler
	CloudEventHandlers map[common.EventType]StreamingCloudEventHandler
}

// ConnectStan connects to Stan
func ConnectStan(natsConfig *NatsConfig, stanConfig *StanConfig) (*StanConnection, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "ConnectStan",
	})

	logger.Tracef("trying to connect to %s", natsConfig.URL)

	// Connect to Stan
	sc, err := stan.Connect(stanConfig.ClusterID, natsConfig.ClientID, stan.NatsURL(natsConfig.URL))
	if err != nil {
		logger.WithError(err).Errorf("failed to connect to %s", natsConfig.URL)
		return nil, err
	}

	stanConn := &StanConnection{
		NatsConfig:         natsConfig,
		StanConfig:         stanConfig,
		Connection:         sc,
		EventHandlers:      map[common.EventType]StreamingEventHandler{},
		CloudEventHandlers: map[common.EventType]StreamingCloudEventHandler{},
	}

	// subscribe a common cyverse event channel
	handler := func(msg *stan.Msg) {
		err := msg.Ack()
		if err != nil {
			logger.Error(err)
		}

		ce, err := ConvertStan(msg)
		if err != nil {
			logger.Error(err)
		} else {
			// handle events
			err := stanConn.callEventHandler(&ce)
			if err != nil {
				logger.Error(err)
			}
		}
	}

	_, err = sc.QueueSubscribe(string(common.StreamingEventSubject), natsConfig.QueueGroup, handler, stan.DurableName(stanConfig.DurableName), stan.SetManualAckMode(), stan.AckWait(DefaultStanAckWaitTime))
	if err != nil {
		logger.Error(err)
		sc.Close()
		return nil, err
	}

	logger.Tracef("established a connection to %s", natsConfig.URL)

	return stanConn, nil
}

// ConnectStanForSender connects to Stan for sending events
func ConnectStanForSender(natsConfig *NatsConfig, stanConfig *StanConfig) (*StanConnection, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "ConnectStanForSender",
	})

	logger.Tracef("trying to connect to %s", natsConfig.URL)

	// Connect to Stan
	sc, err := stan.Connect(stanConfig.ClusterID, natsConfig.ClientID, stan.NatsURL(natsConfig.URL))
	if err != nil {
		logger.WithError(err).Errorf("failed to connect to %s", natsConfig.URL)
		return nil, err
	}

	stanConn := &StanConnection{
		NatsConfig:         natsConfig,
		StanConfig:         stanConfig,
		Connection:         sc,
		EventHandlers:      map[common.EventType]StreamingEventHandler{},
		CloudEventHandlers: map[common.EventType]StreamingCloudEventHandler{},
	}

	logger.Tracef("established a connection to %s", natsConfig.URL)

	return stanConn, nil
}

// Disconnect disconnects Stan connection
func (conn *StanConnection) Disconnect() error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "StanConnection.Disconnect",
	})

	logger.Tracef("trying to disconnect from %s", conn.NatsConfig.URL)

	conn.Connection.Close()

	logger.Tracef("disconnected from %s", conn.NatsConfig.URL)

	// clear
	conn.Connection = nil
	conn.EventHandlers = map[common.EventType]StreamingEventHandler{}
	conn.CloudEventHandlers = map[common.EventType]StreamingCloudEventHandler{}
	return nil
}

// AddEventHandler adds a new event handler function to a specified subject
// eventHandler receives subject and JSON data of an event
func (conn *StanConnection) AddEventHandler(subject common.EventType, eventHandler StreamingEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "StanConnection.AddEventHandler",
	})

	if len(subject) == 0 {
		err := fmt.Errorf("failed to add an event handler for an empty subject")
		return err
	}

	logger.Tracef("adding an event handler for a subject %s", subject)

	conn.EventHandlers[common.EventType(subject)] = eventHandler

	logger.Tracef("added an event handler for a subject %s", subject)

	return nil
}

// AddCloudEventHandler adds a new event handler function to a specified subject
// eventHandler receives a cloudevent of an event
func (conn *StanConnection) AddCloudEventHandler(subject common.EventType, eventHandler StreamingCloudEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "StanConnection.AddCloudEventHandler",
	})

	if len(subject) == 0 {
		err := fmt.Errorf("failed to add an event handler for an empty subject")
		return err
	}

	logger.Tracef("adding an event handler for a subject %s", subject)

	conn.CloudEventHandlers[common.EventType(subject)] = eventHandler

	logger.Tracef("added an event handler for a subject %s", subject)

	return nil
}

// Publish publishes Stan event
func (conn *StanConnection) Publish(subject common.EventType, data interface{}) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "StanConnection.Publish",
	})

	logger.Tracef("publishing a subject %s", subject)

	ce, err := CreateCloudEvent(data, string(subject), conn.NatsConfig.ClientID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event for subject %s", subject)
		return err
	}

	eventJSON, err := ce.MarshalJSON()
	if err != nil {
		logger.WithError(err).Errorf("failed to marshal an event to JSON")
		return err
	}

	err = conn.Connection.Publish(common.StreamingEventSubject, eventJSON)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish a subject %s", ce.Type())
		return err
	}

	logger.Tracef("published a subject %s", ce.Type())

	return nil
}

// PublishWithTransactionID publishes Stan event
// transactionID will be passed along to the CreateCloudEvent
func (conn *StanConnection) PublishWithTransactionID(subject common.EventType, data interface{}, transactionID common.EventID) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "StanConnection.PublishWithTransactionID",
	})

	logger.Tracef("publishing a subject %s", subject)

	ce, err := CreateCloudEventWithTransactionID(data, string(subject), conn.NatsConfig.ClientID, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event for subject %s", subject)
		return err
	}

	eventJSON, err := ce.MarshalJSON()
	if err != nil {
		logger.WithError(err).Errorf("failed to marshal an event to JSON")
		return err
	}

	err = conn.Connection.Publish(common.StreamingEventSubject, eventJSON)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish a subject %s", ce.Type())
		return err
	}

	logger.Tracef("published a subject %s", ce.Type())

	return nil
}

// PublishCloudEvent publishes Stan event
func (conn *StanConnection) PublishCloudEvent(ce *cloudevents.Event) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "StanConnection.PublishCloudEvent",
	})

	// we put a subject into Event.Type instead of Event.Subject
	// please refer 'CreateCloudEvent' function in cloudevent.go
	logger.Tracef("publishing a subject %s", ce.Type())

	eventJSON, err := ce.MarshalJSON()
	if err != nil {
		logger.WithError(err).Errorf("failed to marshal an event to JSON")
		return err
	}

	err = conn.Connection.Publish(common.StreamingEventSubject, eventJSON)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish a subject %s", ce.Type())
		return err
	}

	logger.Tracef("published a subject %s", ce.Type())

	return nil
}

// calls a event handler for an event
func (conn *StanConnection) callEventHandler(event *cloudevents.Event) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "StanConnection.callEventHandler",
	})

	// we put a subject into Event.Type instead of Event.Subject
	// please refer 'CreateCloudEvent' function in cloudevent.go
	logger.Tracef("handling an event %s", event.Type())

	if handler, ok := conn.EventHandlers[common.EventType(event.Type())]; ok {
		// has the handler for the event
		transactionID := GetTransactionID(event)
		err := handler(common.EventType(event.Type()), transactionID, event.Data())
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return err
		}
		return nil
	} else if handler, ok := conn.CloudEventHandlers[common.EventType(event.Type())]; ok {
		// has the handler for the event
		err := handler(event)
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return err
		}
		return nil
	} else {
		// ignore events
		return nil
	}
}
