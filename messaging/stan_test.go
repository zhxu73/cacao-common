// Package messaging contains utility functions for dealing with a massaging service
package messaging

import (
	"encoding/json"
	"testing"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/stretchr/testify/assert"

	"gitlab.com/cyverse/cacao-common/common"
)

type testStanMsgObject struct {
	ID    string
	Field string
}

var testStanConfig StanConfig = StanConfig{
	ClusterID:   "test_custer_id",
	DurableName: "test_durable_name",
}

func TestCreateStanConnection(t *testing.T) {
	conn, err := CreateMockStanConnection(&testNatsConfig, &testStanConfig)
	assert.NoError(t, err)
	assert.NotEmpty(t, conn)
}

func TestDisconnectStanConnection(t *testing.T) {
	conn, err := CreateMockStanConnection(&testNatsConfig, &testStanConfig)
	assert.NoError(t, err)
	assert.NotEmpty(t, conn)

	err = conn.Disconnect()
	assert.NoError(t, err)
}

func TestStanConnectionCastToInterface(t *testing.T) {
	conn, err := CreateMockStanConnection(&testNatsConfig, &testStanConfig)
	assert.NoError(t, err)
	assert.NotEmpty(t, conn)
	assert.Implements(t, (*StreamingEventService)(nil), conn)

	err = conn.Disconnect()
	assert.NoError(t, err)
}

func TestStanSubscribeAndPublish(t *testing.T) {
	conn, err := CreateMockStanConnection(&testNatsConfig, &testStanConfig)
	assert.NoError(t, err)
	assert.NotEmpty(t, conn)

	testSubject := "test_subject"
	testMessage := testStanMsgObject{
		ID:    "0001",
		Field: "test_field1",
	}
	testTransactionID := common.EventID("test_transaction")

	eventHandler := func(subject common.EventType, transactionID common.EventID, jsonData []byte) error {
		assert.Equal(t, testSubject, string(subject))
		assert.Equal(t, testTransactionID, transactionID)

		var receivedMessage testStanMsgObject
		err := json.Unmarshal(jsonData, &receivedMessage)
		assert.NoError(t, err)

		assert.Equal(t, testMessage, receivedMessage)
		return nil
	}

	err = conn.AddEventHandler(common.EventType(testSubject), eventHandler)
	assert.NoError(t, err)

	err = conn.PublishWithTransactionID(common.EventType(testSubject), testMessage, testTransactionID)
	assert.NoError(t, err)

	err = conn.Disconnect()
	assert.NoError(t, err)
}

func TestStanSubscribeAndRequestCloudEvent(t *testing.T) {
	conn, err := CreateMockStanConnection(&testNatsConfig, &testStanConfig)
	assert.NoError(t, err)
	assert.NotEmpty(t, conn)

	testSubject := "test_subject"
	testMessage := testStanMsgObject{
		ID:    "0001",
		Field: "test_field1",
	}
	testClientID := "test_client_id"
	testTransactionID := NewTransactionID()

	ce, err := CreateCloudEventWithTransactionID(testMessage, string(testSubject), testClientID, testTransactionID)
	assert.NoError(t, err)
	assert.NotNil(t, ce)

	eventHandler := func(event *cloudevents.Event) error {
		assert.Equal(t, testSubject, event.Type())
		assert.Equal(t, testClientID, event.Source())

		var receivedMessage testStanMsgObject
		err := json.Unmarshal(event.Data(), &receivedMessage)
		assert.NoError(t, err)

		assert.Equal(t, testMessage, receivedMessage)

		return nil
	}

	err = conn.AddCloudEventHandler(common.EventType(testSubject), eventHandler)
	assert.NoError(t, err)

	err = conn.PublishCloudEvent(&ce)
	assert.NoError(t, err)

	err = conn.Disconnect()
	assert.NoError(t, err)
}
