// Package messaging contains utility functions for dealing with a massaging service
package messaging

import (
	"fmt"
	"regexp"
	"strings"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
)

// MockNatsMessage mocks Nats Msg
type MockNatsMessage struct {
	Data         []byte
	ResponseData []byte
}

// Respond responds
func (msg *MockNatsMessage) Respond(data []byte) error {
	msg.ResponseData = data
	return nil
}

// MockNatsConnection mocks Nats connection
// implements QueryEventService
type MockNatsConnection struct {
	Config             *NatsConfig
	CommonSubjectRegex *regexp.Regexp
	EventHandlers      map[common.QueryOp]QueryEventHandler
	CloudEventHandlers map[common.QueryOp]QueryCloudEventHandler
}

// CreateMockNatsConnection creates MockNatsConnection
func CreateMockNatsConnection(config *NatsConfig) (*MockNatsConnection, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "CreateMockNatsConnection",
	})

	if len(config.WildcardSubject) == 0 {
		err := fmt.Errorf("failed to subscribe an empty subject")
		logger.Error(err)
		return nil, err
	}

	escapedSubject := strings.ReplaceAll(config.WildcardSubject, ".", "\\.")
	wildcard1Regex := strings.ReplaceAll(escapedSubject, "*", "(\\w+)")
	wildcard2Regex := strings.ReplaceAll(wildcard1Regex, ">", "(.+)")

	// test compile
	reg, err := regexp.Compile(wildcard2Regex)
	if err != nil {
		logger.WithError(err).Errorf("failed to compile a subject %s", config.WildcardSubject)
		return nil, err
	}

	mockNatsConn := &MockNatsConnection{
		Config:             config,
		CommonSubjectRegex: reg,
		EventHandlers:      map[common.QueryOp]QueryEventHandler{},
		CloudEventHandlers: map[common.QueryOp]QueryCloudEventHandler{},
	}

	return mockNatsConn, nil
}

// Disconnect disconnects Nats connection
func (conn *MockNatsConnection) Disconnect() error {
	conn.CommonSubjectRegex = nil
	conn.EventHandlers = map[common.QueryOp]QueryEventHandler{}
	conn.CloudEventHandlers = map[common.QueryOp]QueryCloudEventHandler{}
	return nil
}

// AddEventHandler adds a new event handler function to a specified subject. The subject must match the wildcard subject specified in NatsConfig.WildcardSubject
// eventHandler receives subject and JSON data of an event
func (conn *MockNatsConnection) AddEventHandler(subject common.QueryOp, eventHandler QueryEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockNatsConnection.AddEventHandler",
	})

	if len(subject) == 0 {
		err := fmt.Errorf("failed to add an event handler for an empty subject")
		return err
	}

	logger.Tracef("adding an event handler for a subject %s", subject)

	conn.EventHandlers[common.QueryOp(subject)] = eventHandler

	logger.Tracef("added an event handler for a subject %s", subject)
	return nil
}

// AddCloudEventHandler adds a new event handler function to a specified subject. The subject must match the wildcard subject specified in NatsConfig.WildcardSubject
// eventHandler receives a cloudevent of an event
func (conn *MockNatsConnection) AddCloudEventHandler(subject common.QueryOp, eventHandler QueryCloudEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockNatsConnection.AddCloudEventHandler",
	})

	if len(subject) == 0 {
		err := fmt.Errorf("failed to add an event handler for an empty subject")
		return err
	}

	logger.Tracef("adding an event handler for a subject %s", subject)

	conn.CloudEventHandlers[common.QueryOp(subject)] = eventHandler

	logger.Tracef("added an event handler for a subject %s", subject)
	return nil
}

// Request publishes Nats event
func (conn *MockNatsConnection) Request(subject common.QueryOp, data interface{}) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockNatsConnection.Request",
	})

	logger.Tracef("requesting a subject %s", subject)

	ce, err := CreateCloudEvent(data, string(subject), conn.Config.ClientID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event for subject %s", subject)
		return nil, err
	}

	responseMsg, err := conn.request(&ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to request a subject %s", ce.Type())
		return nil, err
	}

	logger.Tracef("requested a subject %s", ce.Type())

	return responseMsg, nil
}

// RequestWithTransactionID publishes Nats event
func (conn *MockNatsConnection) RequestWithTransactionID(subject common.QueryOp, data interface{}, transactionID common.EventID) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockNatsConnection.RequestWithTransactionID",
	})

	logger.Tracef("requesting a subject %s", subject)

	ce, err := CreateCloudEventWithTransactionID(data, string(subject), conn.Config.ClientID, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event for subject %s", subject)
		return nil, err
	}

	responseMsg, err := conn.request(&ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to request a subject %s", ce.Type())
		return nil, err
	}

	logger.Tracef("requested a subject %s", ce.Type())

	return responseMsg, nil
}

// RequestCloudEvent publishes Nats event
func (conn *MockNatsConnection) RequestCloudEvent(ce *cloudevents.Event) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockNatsConnection.RequestCloudEvent",
	})

	// we put a subject into Event.Type instead of Event.Subject
	// please refer 'CreateCloudEvent' function in cloudevent.go
	logger.Tracef("publishing a subject %s", ce.Type())

	responseMsg, err := conn.request(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to request a subject %s", ce.Type())
		return nil, err
	}

	logger.Tracef("requested a subject %s", ce.Type())

	return responseMsg, nil
}

func (conn *MockNatsConnection) request(event *cloudevents.Event) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockNatsConnection.request",
	})

	if conn.CommonSubjectRegex.Match([]byte(event.Type())) {
		// call handler
		if handler, ok := conn.EventHandlers[common.QueryOp(event.Type())]; ok {
			// has the handler for the event
			transactionID := GetTransactionID(event)
			response, err := handler(common.QueryOp(event.Type()), transactionID, event.Data())
			if err != nil {
				logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
				return nil, err
			}
			return response, nil
		} else if handler, ok := conn.CloudEventHandlers[common.QueryOp(event.Type())]; ok {
			// has the handler for the event
			response, err := handler(event)
			if err != nil {
				logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
				return nil, err
			}
			return response, nil
		} else {
			err := fmt.Errorf("failed to find an event handler for a type %s", event.Type())
			logger.Error(err)
			return nil, err
		}
	}
	return nil, nil
}
