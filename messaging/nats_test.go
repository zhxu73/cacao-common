// Package messaging contains utility functions for dealing with a massaging service
package messaging

import (
	"encoding/json"
	"testing"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
)

type testNatsMsgObject struct {
	ID    string
	Field string
}

var testNatsConfig NatsConfig = NatsConfig{
	URL:             "nats://nats:4222",
	QueueGroup:      "test_queue_group",
	WildcardSubject: "test_subject",
	ClientID:        "test_client_id",
	MaxReconnects:   5,
	ReconnectWait:   10,
	RequestTimeout:  10,
}

func TestCreateNatsConnection(t *testing.T) {
	conn, err := CreateMockNatsConnection(&testNatsConfig)
	assert.NoError(t, err)
	assert.NotEmpty(t, conn)
}

func TestDisconnectNatsConnection(t *testing.T) {
	conn, err := CreateMockNatsConnection(&testNatsConfig)
	assert.NoError(t, err)
	assert.NotEmpty(t, conn)

	err = conn.Disconnect()
	assert.NoError(t, err)
}

func TestNatsConnectionCastToInterface(t *testing.T) {
	conn, err := CreateMockNatsConnection(&testNatsConfig)
	assert.NoError(t, err)
	assert.NotEmpty(t, conn)
	assert.Implements(t, (*QueryEventService)(nil), conn)

	err = conn.Disconnect()
	assert.NoError(t, err)
}

func TestNatsSubscribeAndRequest(t *testing.T) {
	testNatsConfigEmptySubject := NatsConfig{
		URL:             "nats://nats:4222",
		QueueGroup:      "test_queue_group",
		WildcardSubject: "test_subject.>",
		ClientID:        "test_client_id",
	}

	conn, err := CreateMockNatsConnection(&testNatsConfigEmptySubject)
	assert.NoError(t, err)
	assert.NotEmpty(t, conn)

	testSubject := "test_subject.test1"
	testMessage := testNatsMsgObject{
		ID:    "0001",
		Field: "test_field1",
	}

	testResponse := "test_response"

	eventHandler := func(subject common.QueryOp, transactionID common.EventID, jsonData []byte) ([]byte, error) {
		assert.Equal(t, testSubject, string(subject))
		assert.Empty(t, transactionID)

		var receivedMessage testNatsMsgObject
		err := json.Unmarshal(jsonData, &receivedMessage)
		assert.NoError(t, err)

		assert.Equal(t, testMessage, receivedMessage)

		return []byte(testResponse), nil
	}

	err = conn.AddEventHandler(common.QueryOp(testSubject), eventHandler)
	assert.NoError(t, err)

	response, err := conn.Request(common.QueryOp(testSubject), testMessage)
	assert.NoError(t, err)
	assert.Equal(t, []byte(testResponse), response)

	err = conn.Disconnect()
	assert.NoError(t, err)
}

func TestNatsSubscribeAndRequestCloudEvent(t *testing.T) {
	testNatsConfigEmptySubject := NatsConfig{
		URL:             "nats://nats:4222",
		QueueGroup:      "test_queue_group",
		WildcardSubject: "test_subject.>",
		ClientID:        "test_client_id",
	}

	conn, err := CreateMockNatsConnection(&testNatsConfigEmptySubject)
	assert.NoError(t, err)
	assert.NotEmpty(t, conn)

	testSubject := "test_subject.test1"
	testMessage := testNatsMsgObject{
		ID:    "0001",
		Field: "test_field1",
	}
	testClientID := "test_client_id"

	ce, err := CreateCloudEvent(testMessage, string(testSubject), testClientID)
	assert.NoError(t, err)
	assert.NotNil(t, ce)

	testResponse := "test_response"

	eventHandler := func(event *cloudevents.Event) ([]byte, error) {
		assert.Equal(t, testSubject, event.Type())
		assert.Equal(t, testClientID, event.Source())

		var receivedMessage testNatsMsgObject
		err := json.Unmarshal(event.Data(), &receivedMessage)
		assert.NoError(t, err)

		assert.Equal(t, testMessage, receivedMessage)

		return []byte(testResponse), nil
	}

	err = conn.AddCloudEventHandler(common.QueryOp(testSubject), eventHandler)
	assert.NoError(t, err)

	response, err := conn.RequestCloudEvent(&ce)
	assert.NoError(t, err)
	assert.Equal(t, []byte(testResponse), response)

	err = conn.Disconnect()
	assert.NoError(t, err)
}
