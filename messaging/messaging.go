// Package messaging contains utility functions for dealing with a massaging service
package messaging

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
)

// QueryEventHandler is a function prototype for query event handlers
type QueryEventHandler func(subject common.QueryOp, transactionID common.EventID, jsonData []byte) ([]byte, error)

// QueryCloudEventHandler is a function prototype for query event handlers
type QueryCloudEventHandler func(event *cloudevents.Event) ([]byte, error)

// StreamingEventHandler is a function prototype for streaming event handlers
type StreamingEventHandler func(subject common.EventType, transactionID common.EventID, jsonData []byte) error

// StreamingCloudEventHandler is a function prototype for streaming event handlers
type StreamingCloudEventHandler func(event *cloudevents.Event) error

// QueryEventService is an interface for a query event service (e.g., Nats)
type QueryEventService interface {
	Disconnect() error
	AddEventHandler(subject common.QueryOp, eventHandler QueryEventHandler) error
	AddCloudEventHandler(subject common.QueryOp, eventHandler QueryCloudEventHandler) error
	Request(subject common.QueryOp, data interface{}) ([]byte, error)
	RequestWithTransactionID(subject common.QueryOp, data interface{}, transactionID common.EventID) ([]byte, error)
	RequestCloudEvent(ce *cloudevents.Event) ([]byte, error)
}

// StreamingEventService is an interface for a streaming event service (e.g., Stan)
type StreamingEventService interface {
	Disconnect() error
	AddEventHandler(subject common.EventType, eventHandler StreamingEventHandler) error
	AddCloudEventHandler(subject common.EventType, eventHandler StreamingCloudEventHandler) error
	Publish(subject common.EventType, data interface{}) error
	PublishWithTransactionID(subject common.EventType, data interface{}, transactionID common.EventID) error
	PublishCloudEvent(ce *cloudevents.Event) error
}
