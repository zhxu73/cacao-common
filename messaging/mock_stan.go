// Package messaging contains utility functions for dealing with a massaging service
package messaging

import (
	"fmt"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
)

// MockStanConnection mocks Stan connection
// implements StreamingEventService
type MockStanConnection struct {
	NatsConfig         *NatsConfig
	StanConfig         *StanConfig
	EventHandlers      map[common.EventType]StreamingEventHandler
	CloudEventHandlers map[common.EventType]StreamingCloudEventHandler
}

// CreateMockStanConnection creates MockStanConnection
func CreateMockStanConnection(natsConfig *NatsConfig, stanConfig *StanConfig) (*MockStanConnection, error) {
	mockStanConn := &MockStanConnection{
		NatsConfig:         natsConfig,
		StanConfig:         stanConfig,
		EventHandlers:      map[common.EventType]StreamingEventHandler{},
		CloudEventHandlers: map[common.EventType]StreamingCloudEventHandler{},
	}

	return mockStanConn, nil
}

// Disconnect disconnects Stan connection
func (conn *MockStanConnection) Disconnect() error {
	conn.EventHandlers = map[common.EventType]StreamingEventHandler{}
	conn.CloudEventHandlers = map[common.EventType]StreamingCloudEventHandler{}
	return nil
}

// AddEventHandler adds a new event handler function to a specified subject
// eventHandler receives subject and JSON data of an event
func (conn *MockStanConnection) AddEventHandler(subject common.EventType, eventHandler StreamingEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockStanConnection.AddEventHandler",
	})

	if len(subject) == 0 {
		err := fmt.Errorf("failed to add an event handler for an empty subject")
		return err
	}

	logger.Tracef("adding an event handler for a subject %s", subject)

	conn.EventHandlers[common.EventType(subject)] = eventHandler

	logger.Tracef("added an event handler for a subject %s", subject)
	return nil
}

// AddCloudEventHandler adds a new event handler function to a specified subject
// eventHandler receives a cloudevent of an event
func (conn *MockStanConnection) AddCloudEventHandler(subject common.EventType, eventHandler StreamingCloudEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockStanConnection.SubscribeCloudEvent",
	})

	if len(subject) == 0 {
		err := fmt.Errorf("failed to add an event handler for an empty subject")
		return err
	}

	logger.Tracef("adding an event handler for a subject %s", subject)

	conn.CloudEventHandlers[common.EventType(subject)] = eventHandler

	logger.Tracef("added an event handler for a subject %s", subject)
	return nil
}

// Publish publishes Stan event
func (conn *MockStanConnection) Publish(subject common.EventType, data interface{}) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockStanConnection.Publish",
	})

	logger.Tracef("publishing a subject %s", subject)

	ce, err := CreateCloudEvent(data, string(subject), conn.NatsConfig.ClientID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event for subject %s", subject)
		return err
	}

	err = conn.publish(&ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish a subject %s", ce.Type())
		return err
	}

	logger.Tracef("published a subject %s", ce.Type())

	return nil
}

// PublishWithTransactionID publishes Stan event
func (conn *MockStanConnection) PublishWithTransactionID(subject common.EventType, data interface{}, transactionID common.EventID) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockStanConnection.Publish",
	})

	logger.Tracef("publishing a subject %s", subject)

	ce, err := CreateCloudEventWithTransactionID(data, string(subject), conn.NatsConfig.ClientID, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event for subject %s", subject)
		return err
	}

	err = conn.publish(&ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish a subject %s", ce.Type())
		return err
	}

	logger.Tracef("published a subject %s", ce.Type())

	return nil
}

// PublishCloudEvent publishes Stan event
func (conn *MockStanConnection) PublishCloudEvent(ce *cloudevents.Event) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockStanConnection.PublishCloudEvent",
	})

	// we put a subject into Event.Type instead of Event.Subject
	// please refer 'CreateCloudEvent' function in cloudevent.go
	logger.Tracef("publishing a subject %s", ce.Type())

	err := conn.publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to request a subject %s", ce.Type())
		return err
	}

	logger.Tracef("published a subject %s", ce.Type())

	return nil
}

func (conn *MockStanConnection) publish(event *cloudevents.Event) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockStanConnection.publish",
	})

	if handler, ok := conn.EventHandlers[common.EventType(event.Type())]; ok {
		// has the handler for the event
		transactionID := GetTransactionID(event)
		err := handler(common.EventType(event.Type()), transactionID, event.Data())
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return err
		}
		return nil
	} else if handler, ok := conn.CloudEventHandlers[common.EventType(event.Type())]; ok {
		// has the handler for the event
		err := handler(event)
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return err
		}
		return nil
	} else {
		// ignore events
		return nil
	}
}
