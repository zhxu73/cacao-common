// Package messaging -- this file contains utility functions for dealing with CloudEvents
package messaging

import (
	"encoding/json"
	"time"

	"github.com/rs/xid"
	"gitlab.com/cyverse/cacao-common/common"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	nats "github.com/nats-io/nats.go"
	stan "github.com/nats-io/stan.go"
)

// CreateCloudEvent takes any object, eventType string, source string, and creates a resulting CloudEvent
// This utility provides the following conveniences:
// * uniformly assigns a new id of the format "cloudevent-" + xid
// * sets the time to UTC
// * generically marshals the data to json and appropriately assigns the cloudevent type
// * creates or sets a transaction id, which can later be used to pair requests to responses or corresponding events
func CreateCloudEvent(data interface{}, eventType string, source string) (cloudevents.Event, error) {
	event := cloudevents.NewEvent()
	event.SetID("cloudevent-" + xid.New().String())
	event.SetType(eventType)
	event.SetTime(time.Now().UTC())
	event.SetSource(source)
	b, err := json.Marshal(data)
	event.SetData(cloudevents.ApplicationJSON, b)

	return event, err
}

// CreateCloudEventWithTransactionID creates a CloudEvent with given transactionID, transactionID is optional
func CreateCloudEventWithTransactionID(data interface{}, eventType string, source string, transactionID common.EventID) (cloudevents.Event, error) {
	event := cloudevents.NewEvent()
	event.SetID("cloudevent-" + xid.New().String())
	event.SetType(eventType)
	event.SetTime(time.Now().UTC())
	event.SetSource(source)
	if len(transactionID) > 0 {
		event.SetExtension("TransactionID", string(transactionID))
	}
	b, err := json.Marshal(data)
	event.SetData(cloudevents.ApplicationJSON, b)

	return event, err
}

// NewTransactionID is a utility function that will generate a CACAO-specific transaction ID of the form transaction-<xid>
// Replies and events generated from a specific request should include the transaction id for easier matching of requests
func NewTransactionID() common.EventID {
	return common.EventID("tid-" + xid.New().String())
}

// SetTransactionID is a utility function that writes TransactionID to CloudEvent quick
func SetTransactionID(ce *cloudevents.Event, transactionID common.EventID) {
	ce.SetExtension("TransactionID", string(transactionID))
}

// GetTransactionID is a utility function that reads TransactionID from CloudEvent quick
func GetTransactionID(ce *cloudevents.Event) common.EventID {
	transactionID, err := ce.Context.GetExtension("TransactionID")
	if err != nil {
		// return empty string when the event doesn't have transaction ID
		return ""
	}

	transactionIDString := transactionID.(string)
	return common.EventID(transactionIDString)
}

// ConvertNats converts NATS message to CloudEvents message
func ConvertNats(msg *nats.Msg) (cloudevents.Event, error) {
	event := cloudevents.NewEvent()
	err := json.Unmarshal(msg.Data, &event)
	return event, err
}

// ConvertStan converts STAN message to CloudEvents message
func ConvertStan(msg *stan.Msg) (cloudevents.Event, error) {
	event := cloudevents.NewEvent()
	err := json.Unmarshal(msg.Data, &event)
	return event, err
}
