// Package messaging contains utility functions for dealing with a massaging service
package messaging

import (
	"fmt"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	nats "github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
)

const (
	// DefaultNatsURL is a default nats URL
	DefaultNatsURL = "nats://nats:4222"
	// DefaultNatsClusterID is a default Cluster ID for cacao
	DefaultNatsClusterID = "cacao-cluster"
	// DefaultNatsMaxReconnect is default max reconnect trials
	DefaultNatsMaxReconnect = 6 // max times to reconnect within nats.connect()
	// DefaultNatsReconnectWait is a default delay for next reconnect
	DefaultNatsReconnectWait = 10 * time.Second // seconds to wait within nats.connect()
	// DefaultNatsRequestTimeout is default timeout for requests
	DefaultNatsRequestTimeout = 5 * time.Second // timeout for requests
)

// NatsConfig stores configurations used by both nats query channel and nats streaming
type NatsConfig struct {
	URL             string `envconfig:"NATS_URL" default:"nats://nats:4222"`
	QueueGroup      string `envconfig:"NATS_QUEUE_GROUP"`
	WildcardSubject string `envconfig:"NATS_WILDCARD_SUBJECT" default:"cyverse.>"` // WildcardSubject field is optional, only used for NATS Query
	ClientID        string `envconfig:"NATS_CLIENT_ID"`                            // While not strictly used by Nats, this is propagated into the cloudevent
	MaxReconnects   int    `envconfig:"NATS_MAX_RECONNECTS" default:"-1"`          // implementation should default to DefaultNatsMaxReconnect
	ReconnectWait   int    `envconfig:"NATS_RECONNECT_WAIT" default:"-1"`          // in seconds, implementation should default to
	RequestTimeout  int    `envconfig:"NATS_REQUEST_TIMEOUT" default:"-1"`
}

// NatsConnection contains Nats connection info
type NatsConnection struct {
	Config             *NatsConfig
	Connection         *nats.Conn
	EventHandlers      map[common.QueryOp]QueryEventHandler
	CloudEventHandlers map[common.QueryOp]QueryCloudEventHandler
}

// ConnectNats connects to Nats
func ConnectNats(config *NatsConfig) (*NatsConnection, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "ConnectNats",
	})

	logger.Tracef("trying to connect to %s", config.URL)

	options := []nats.Option{}

	if config.MaxReconnects >= 0 {
		options = append(options, nats.MaxReconnects(config.MaxReconnects))
	}

	if config.ReconnectWait >= 0 {
		reconnectWait := time.Duration(config.ReconnectWait) * time.Second
		options = append(options, nats.ReconnectWait(reconnectWait))
	}

	// Connect to Nats
	nc, err := nats.Connect(config.URL, options...)
	if err != nil {
		logger.WithError(err).Errorf("failed to connect to %s", config.URL)
		return nil, err
	}

	natsConn := &NatsConnection{
		Config:             config,
		Connection:         nc,
		EventHandlers:      map[common.QueryOp]QueryEventHandler{},
		CloudEventHandlers: map[common.QueryOp]QueryCloudEventHandler{},
	}

	if len(config.WildcardSubject) == 0 {
		err := fmt.Errorf("failed to subscribe an empty subject")
		logger.Error(err)
		nc.Close()
		return nil, err
	}

	// Add a handler
	handler := func(msg *nats.Msg) {
		ce, err := ConvertNats(msg)
		if err != nil {
			logger.Error(err)
		} else {
			// handle events
			response, err := natsConn.callEventHandler(&ce)
			if err != nil {
				logger.Error(err)
			} else {
				err = msg.Respond(response)
				if err != nil {
					logger.Error(err)
				}
			}
		}
	}

	// subject should contain wildcards ("*" or ">")
	_, err = nc.QueueSubscribe(config.WildcardSubject, config.QueueGroup, handler)
	if err != nil {
		logger.Error(err)
		nc.Close()
		return nil, err
	}

	logger.Tracef("established a connection to %s", config.URL)

	return natsConn, nil
}

// ConnectNatsForSender connects to Nats for sending events
func ConnectNatsForSender(config *NatsConfig) (*NatsConnection, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "ConnectNatsForSender",
	})

	logger.Tracef("trying to connect to %s", config.URL)

	options := []nats.Option{}

	if config.MaxReconnects >= 0 {
		options = append(options, nats.MaxReconnects(config.MaxReconnects))
	}

	if config.ReconnectWait >= 0 {
		reconnectWait := time.Duration(config.ReconnectWait) * time.Second
		options = append(options, nats.ReconnectWait(reconnectWait))
	}

	// Connect to Nats
	nc, err := nats.Connect(config.URL, options...)
	if err != nil {
		logger.WithError(err).Errorf("failed to connect to %s", config.URL)
		return nil, err
	}

	natsConn := &NatsConnection{
		Config:             config,
		Connection:         nc,
		EventHandlers:      map[common.QueryOp]QueryEventHandler{},
		CloudEventHandlers: map[common.QueryOp]QueryCloudEventHandler{},
	}

	logger.Tracef("established a connection to %s", config.URL)

	return natsConn, nil
}

// Disconnect disconnects Nats connection
func (conn *NatsConnection) Disconnect() error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "NatsConnection.Disconnect",
	})

	logger.Tracef("trying to disconnect from %s", conn.Config.URL)

	if conn.Connection == nil {
		err := fmt.Errorf("connection is not established")
		logger.Error(err)
		return err
	}

	if conn.Connection.IsConnected() {
		conn.Connection.Close()
	}

	logger.Tracef("disconnected from %s", conn.Config.URL)

	// clear
	conn.Connection = nil
	conn.EventHandlers = map[common.QueryOp]QueryEventHandler{}
	conn.CloudEventHandlers = map[common.QueryOp]QueryCloudEventHandler{}
	return nil
}

// AddEventHandler adds a new event handler function to a specified subject. The subject must match the wildcard subject specified in NatsConfig.WildcardSubject
// eventHandler receives subject and JSON data of an event
func (conn *NatsConnection) AddEventHandler(subject common.QueryOp, eventHandler QueryEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "NatsConnection.AddEventHandler",
	})

	if len(subject) == 0 {
		err := fmt.Errorf("failed to add an event handler for an empty subject")
		return err
	}

	logger.Tracef("adding an event handler for a subject %s", subject)

	conn.EventHandlers[common.QueryOp(subject)] = eventHandler

	logger.Tracef("added an event handler for a subject %s", subject)
	return nil
}

// AddCloudEventHandler adds a new event handler function to a specified subject. The subject must match the wildcard subject specified in NatsConfig.WildcardSubject
// eventHandler receives a cloudevent of an event
func (conn *NatsConnection) AddCloudEventHandler(subject common.QueryOp, eventHandler QueryCloudEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "NatsConnection.AddCloudEventHandler",
	})

	if len(subject) == 0 {
		err := fmt.Errorf("failed to add an event handler for an empty subject")
		return err
	}

	logger.Tracef("adding an event handler for a subject %s", subject)

	conn.CloudEventHandlers[common.QueryOp(subject)] = eventHandler

	logger.Tracef("added an event handler for a subject %s", subject)
	return nil
}

// Request publishes Nats event
func (conn *NatsConnection) Request(subject common.QueryOp, data interface{}) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "NatsConnection.Request",
	})

	logger.Tracef("requesting a subject %s", subject)

	ce, err := CreateCloudEvent(data, string(subject), conn.Config.ClientID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event for subject %s", subject)
		return nil, err
	}

	eventJSON, err := ce.MarshalJSON()
	if err != nil {
		logger.WithError(err).Errorf("failed to marshal an event to JSON")
		return nil, err
	}

	requestTimeout := time.Duration(conn.Config.RequestTimeout) * time.Second
	responseMsg, err := conn.Connection.Request(ce.Type(), eventJSON, requestTimeout)
	if err != nil {
		logger.WithError(err).Errorf("failed to request a subject %s", ce.Type())
		return nil, err
	}

	logger.Tracef("requested a subject %s", ce.Type())

	return responseMsg.Data, nil
}

// RequestWithTransactionID publishes Nats event
// transactionID will be passed along to the CreateCloudEvent if given
func (conn *NatsConnection) RequestWithTransactionID(subject common.QueryOp, data interface{}, transactionID common.EventID) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "NatsConnection.RequestWithTransactionID",
	})

	logger.Tracef("requesting a subject %s", subject)

	ce, err := CreateCloudEventWithTransactionID(data, string(subject), conn.Config.ClientID, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event for subject %s", subject)
		return nil, err
	}

	eventJSON, err := ce.MarshalJSON()
	if err != nil {
		logger.WithError(err).Errorf("failed to marshal an event to JSON")
		return nil, err
	}

	requestTimeout := time.Duration(conn.Config.RequestTimeout) * time.Second
	responseMsg, err := conn.Connection.Request(ce.Type(), eventJSON, requestTimeout)
	if err != nil {
		logger.WithError(err).Errorf("failed to request a subject %s", ce.Type())
		return nil, err
	}

	logger.Tracef("requested a subject %s", ce.Type())

	return responseMsg.Data, nil
}

// RequestCloudEvent publishes Nats event
func (conn *NatsConnection) RequestCloudEvent(ce *cloudevents.Event) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "NatsConnection.RequestCloudEvent",
	})

	// we put a subject into Event.Type instead of Event.Subject
	// please refer 'CreateCloudEvent' function in cloudevent.go
	logger.Tracef("publishing a subject %s", ce.Type())
	eventJSON, err := ce.MarshalJSON()
	if err != nil {
		logger.WithError(err).Errorf("failed to marshal an event to JSON")
		return nil, err
	}

	requestTimeout := time.Duration(conn.Config.RequestTimeout) * time.Second
	responseMsg, err := conn.Connection.Request(ce.Type(), eventJSON, requestTimeout)
	if err != nil {
		logger.WithError(err).Errorf("failed to request a subject %s", ce.Type())
		return nil, err
	}

	logger.Tracef("requested a subject %s", ce.Type())

	return responseMsg.Data, nil
}

// calls a event handler for an event and returns response
func (conn *NatsConnection) callEventHandler(event *cloudevents.Event) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "NatsConnection.callEventHandler",
	})

	// we put a subject into Event.Type instead of Event.Subject
	// please refer 'CreateCloudEvent' function in cloudevent.go
	logger.Tracef("handling an event %s", event.Type())

	if handler, ok := conn.EventHandlers[common.QueryOp(event.Type())]; ok {
		// has the handler for the event
		transactionID := GetTransactionID(event)
		response, err := handler(common.QueryOp(event.Type()), transactionID, event.Data())
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return nil, err
		}
		return response, nil
	} else if handler, ok := conn.CloudEventHandlers[common.QueryOp(event.Type())]; ok {
		// has the handler for the event
		response, err := handler(event)
		if err != nil {
			logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
			return nil, err
		}
		return response, nil
	} else {
		err := fmt.Errorf("failed to find an event handler for a type %s", event.Type())
		logger.Error(err)
		return nil, err
	}
}
