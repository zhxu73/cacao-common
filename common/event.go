package common

// QueryOp is a queue for a query operation
type QueryOp string

// EventType is a type to represent event operations
type EventType string

// EventID is the ID of event, this is used to track trasaction
type EventID string

// StreamingEventSubject is the event channel that all cyverse events are posted
const StreamingEventSubject = "org.cyverse.events"

// EventTypePrefix is the prefix for EventType
const EventTypePrefix = StreamingEventSubject + "."

// QueryOpPrefix is the prefix for QueryOp
const NatsQueryOpPrefix = "cyverse."
