package service

import (
	"context"
	"encoding/json"
	"errors"
	"strconv"
	"time"

	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"

	nats "github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
)

// Nats Subjects root, get, list and other channels
const (
	NatsSubjectUsers     string = common.NatsQueryOpPrefix + "users"
	NatsSubjectUsersGet  string = NatsSubjectUsers + ".get"
	NatsSubjectUsersList string = NatsSubjectUsers + ".list"

	EventUserAddRequested    common.EventType = "org.cyverse.events.UserAddRequested"
	EventUserUpdateRequested common.EventType = "org.cyverse.events.UserUpdateRequested"
	EventUserDeleteRequested common.EventType = "org.cyverse.events.UserDeleteRequested"

	EventUserAdded   common.EventType = "org.cyverse.events.UserAdded"
	EventUserUpdated common.EventType = "org.cyverse.events.UserUpdated"
	EventUserDeleted common.EventType = "org.cyverse.events.UserDeleted"

	EventUserAddError    common.EventType = "org.cyverse.events.UserAddError"
	EventUserUpdateError common.EventType = "org.cyverse.events.UserUpdateError"
	EventUserDeleteError common.EventType = "org.cyverse.events.UserDeleteError"
)

// // These are User options
// type NatsUserOptions func(u *natsuserclient)

// func ForcePreferencesOption(user *natsuserclient) { // this option will force the loading and saving of preferences with a User object or User objects in the list
// 	user.User.ForceLoadPreferences = true
// }

// // These are UserList options
// type NatsUserListOptions func(u *NatsUserList)

// func ForcePreferencesListOption(ulist *NatsUserList) { // this option will force the loading and saving of preferences with a User object or User objects in the list
// 	ulist.UserList.ForceLoadPreferences = true
// }

// This object should never be serialized/marshalled
type natsUserClient struct {
	actor          string
	emulator       string
	clientID       string
	clusterID      string // used for stan
	natsUrl        string
	maxReconnect   int
	reconnectWait  time.Duration
	requestTimeout time.Duration
	eventsChannel  string
	eventsTimeout  time.Duration
	ctx            context.Context
}

func NewNatsUserClient(ctx context.Context, actor string, emulator string, natsConfig messaging.NatsConfig, stanConfig messaging.StanConfig) (UserClient, error) {
	log.Trace("NewNatsUserClient() called")

	if actor == "" {
		return nil, errors.New(UserActorNotSetError)
	}

	if ctx == nil {
		ctx = context.Background()
	}
	new_user_svc := natsUserClient{
		actor:         actor,
		emulator:      emulator,
		natsUrl:       natsConfig.URL,
		clientID:      natsConfig.ClientID,
		clusterID:     stanConfig.ClusterID,
		eventsChannel: messaging.DefaultStanEventsSubject, // for now use default
		ctx:           ctx}

	log.Trace("NewNatsUserClient: natsUrl: " + new_user_svc.natsUrl)
	log.Trace("NewNatsUserClient: clientID: " + new_user_svc.clientID)
	log.Trace("NewNatsUserClient: clusterID: " + new_user_svc.clusterID)
	log.Trace("NewNatsUserClient: eventsChannel: " + new_user_svc.eventsChannel)

	// for _, option := range options { // call the options
	// 	option(&new_user_svc)
	// }

	if natsConfig.MaxReconnects <= 0 {
		new_user_svc.maxReconnect = messaging.DefaultNatsMaxReconnect
	} else {
		new_user_svc.maxReconnect = natsConfig.MaxReconnects
	}
	if natsConfig.ReconnectWait <= 0 {
		new_user_svc.reconnectWait = messaging.DefaultNatsReconnectWait
	} else {
		new_user_svc.reconnectWait = time.Duration(natsConfig.ReconnectWait) * time.Second
	}
	if natsConfig.RequestTimeout <= 0 {
		new_user_svc.requestTimeout = messaging.DefaultNatsRequestTimeout
	} else {
		new_user_svc.requestTimeout = time.Duration(natsConfig.RequestTimeout) * time.Second
	}
	if stanConfig.EventsTimeout <= 0 {
		new_user_svc.eventsTimeout = messaging.DefaultStanEventsTimeout
	} else {
		new_user_svc.eventsTimeout = time.Duration(stanConfig.EventsTimeout) * time.Second
	}

	return &new_user_svc, nil
}

func (usersvc *natsUserClient) Add(user User) error {
	log.Trace("natsuserclient.Add() started")
	return usersvc.publishRequestEvent(user, EventUserAddRequested, true)
}

func (usersvc *natsUserClient) Update(user User) error {
	log.Trace("natsuserclient.Update() started")
	return usersvc.publishRequestEvent(user, EventUserUpdateRequested, true)
}

// save will do the logic for saving a user whether it's a create or update, but also allow for async operation
func (usersvc *natsUserClient) publishRequestEvent(user User, eventOpType common.EventType, waitForCompletion bool) error {
	var u *UserModel = user.(*UserModel)
	log.Trace("natsuserclient.publishRequestEvent(" + u.Username + ", " + string(eventOpType) + ", waitForCompletion=" +
		strconv.FormatBool(waitForCompletion) + ")")

	if u.Username == "" {
		return errors.New(UserUsernameNotSetError)
	}

	// let's set session actor and emulator from svc object
	u.SessionActor = usersvc.actor
	u.SessionEmulator = usersvc.actor

	log.Trace("natsuserclient.publishRequestEvent: stan connect for request")
	sc, err := stan.Connect(usersvc.clusterID, usersvc.clientID, stan.NatsURL(usersvc.natsUrl))
	if err != nil {
		return err
	}
	defer sc.Close()

	log.Trace("natsuserclient.publishRequestEvent: creating cloud event")
	transactionID := messaging.NewTransactionID()
	ce, err := messaging.CreateCloudEventWithTransactionID(user, string(eventOpType), usersvc.clientID, transactionID)
	if err != nil {
		return err
	}
	payload, err := json.Marshal(ce)
	if err != nil {
		return err
	}

	if waitForCompletion {

		// note, since we need to handle a custom cancellation process, rather than
		// a simple cancel or timeout through context, let's just setup a context with
		// cancel enabled
		ctx, cancel := context.WithCancel(usersvc.ctx)
		foundchan := make(chan string) // channel, if found -- if not empty string, then there was probably an error

		// This go routine will wait for result of the events request synchronous function
		newClientID := usersvc.clientID + "-" + xid.New().String()
		log.Trace("natsuserclient.publishRequestEvent: starting event request sync function, clientid = " + newClientID)

		// we cannot connect as the same client; so, generate a new client id
		log.Trace("natsuserclient.publishRequestEvent: stan connect for receiving event request (" + newClientID + ")")
		sc2, err := stan.Connect(usersvc.clusterID, newClientID, stan.NatsURL(usersvc.natsUrl))
		if err != nil {
			cancel()
			return err
		}
		defer sc2.Close()

		log.Trace("natsuserclient.publishRequestEvent: starting the subscription (" + newClientID + ")")
		sub, err := sc.Subscribe(usersvc.eventsChannel, func(m *stan.Msg) {
			log.Trace("natsuserclient.publishRequestEvent: within stan function (" + newClientID + ")")

			m.Ack()

			ce, e := messaging.ConvertStan(m)
			if e == nil {
				log.Trace("natsuserclient.publishRequestEvent: converted cloudevent, type = " + ce.Type() + " (" + newClientID + ")")
				var b []byte = nil
				switch eventOpType {
				case EventUserAddRequested:
					if ce.Type() == string(EventUserAdded) || ce.Type() == string(EventUserAddError) {
						b = m.Data
					}
				case EventUserUpdateRequested:
					if ce.Type() == string(EventUserUpdated) || ce.Type() == string(EventUserUpdateError) {
						b = m.Data
					}
				case EventUserDeleteRequested:
					if ce.Type() == string(EventUserDeleted) || ce.Type() == string(EventUserDeleteError) {
						b = m.Data
					}
				}

				// at this point, the byte array was assigned it is relevant to look at
				if b != nil {
					log.Trace("natsuserclient.publishRequestEvent: converted cloudevent, found matching event (" + newClientID + ")")
					eventUser := UserModel{}
					if json.Unmarshal(ce.Data(), &eventUser) == nil { // at this point, we have the user
						if eventUser.Username == u.Username { // u was declared at the beginning of publishRequestEvent
							log.Trace("natsuserclient.publishRequestEvent: found the user " + u.Username + " (" + newClientID + ")")
							foundchan <- eventUser.ErrorType // send what's found in the Error
						}
					}
				}
			}

		}, stan.StartWithLastReceived())
		if err != nil {
			cancel()
			return err
		}
		defer sub.Unsubscribe()

		log.Trace("natsuserclient.publishRequestEvent: sync publishing the event " + eventOpType)
		err = sc.Publish(usersvc.eventsChannel, payload)
		if err != nil {
			cancel()
			return err
		}

		// this is where we wait for a result or just timeout
		log.Trace("natsuserclient.publishRequestEvent: waiting for result (" + newClientID + ")")
		var errorResult error = nil
		select {
		case errorString := <-foundchan:
			if errorString != "" {
				errorResult = errors.New(errorString)
			}
		case <-time.After(usersvc.eventsTimeout):
			errorResult = errors.New(GeneralEventOpTimeoutError)
		case <-ctx.Done():
			errorResult = errors.New(GeneralEventOpCanceledError)
		}
		cancel() // completing the cancel of the context

		return errorResult
	} else {
		log.Trace("natsuserclient.publishRequestEvent: async publishing the event " + eventOpType)
		err = sc.Publish(usersvc.eventsChannel, payload)
		if err != nil {
			return err
		}
	}

	return nil
}

// NewNatsUser creates a new NatsUser struct based on the nats connection; developers will still need
// to Load() after creating a new NatsUser
// actor is the username of the person making requests
// source is a string representing the source/service that is invoking the request (used for cloudevent)
// ctx can be nil, if one isn't available
func (usersvc *natsUserClient) Get(username string) (User, error) {
	log.Trace("natsuserclient.Get() started")

	if username == "" {
		return nil, errors.New(UserUsernameNotSetError)
	}

	log.Trace("natsuserclient.Get: connecting to nats, " + usersvc.natsUrl)
	nc, err := nats.Connect(usersvc.natsUrl, nats.MaxReconnects(usersvc.maxReconnect), nats.ReconnectWait(usersvc.reconnectWait))
	if err != nil {
		return nil, err
	}
	defer nc.Close()

	log.Trace("natsuserclient.Get: creating cloud event")
	user := UserModel{Session: Session{SessionActor: usersvc.actor, SessionEmulator: usersvc.emulator},
		Username: username}
	ce, err := messaging.CreateCloudEvent(user, NatsSubjectUsersGet, usersvc.clientID)
	if err != nil {
		return nil, err
	}
	payload, err := json.Marshal(ce)
	if err != nil {
		return nil, err
	}

	// retrieve the loaded user from the nats ether
	log.Trace("natsuserclient.Get: sending request with context to " + NatsSubjectUsersGet)
	newctx, cancel := context.WithTimeout(usersvc.ctx, usersvc.requestTimeout)
	msg, err := nc.RequestWithContext(newctx, NatsSubjectUsersGet, payload)
	cancel() // no longer need the context, so cancel at this point
	if err != nil {
		return nil, err
	}

	// convert the message to a cloud event
	log.Trace("natsuserclient.Get: received event, converting and unmarshalling")
	ce, err = messaging.ConvertNats(msg)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(ce.Data(), &user)
	if err != nil {
		return nil, err
	}

	// final check for error
	if user.ErrorType != "" {
		log.Trace("natsUserClient.Get: error was received:" + user.ErrorType)
		err = errors.New(user.ErrorType)
	}

	return &user, err
}

func (usersvc *natsUserClient) Delete(user User) error {
	log.Trace("natsUserClient.Delete() started")
	return usersvc.publishRequestEvent(user, EventUserDeleteRequested, true)
}

func (usersvc *natsUserClient) Search(filter UserListFilter) (UserList, error) {
	log.Trace("natsUserClient.Search() started")
	ul := &UserListModel{Session: Session{SessionActor: usersvc.actor, SessionEmulator: usersvc.emulator},
		Filter: filter}

	err := usersvc.load(ul)
	if err != nil {
		ul = nil
	}
	return ul, err
}

func (usersvc *natsUserClient) SearchNext(ul UserList) error {
	log.Trace("natsUserClient.SearchNext() started")

	var ulm *UserListModel = ul.(*UserListModel)

	if ulm.NextStart < 0 {
		return errors.New(UserListLoadBoundaryError)
	}

	// just set the filter index to the new starting point
	ulm.Filter.Start = ulm.NextStart

	return usersvc.load(ulm)
}

func (usersvc natsUserClient) load(ul *UserListModel) error {
	log.Trace("natsUserClient.load() started")

	// TODO: replace when implemented
	return errors.New(GeneralMethodNotYetImplemented)
	/*
		if ul.Filter == (UserListFilter{}) { // compare with an uninitialized UserListFilter
			return errors.New(UserListFilterNotSetError)
		}
		if ul.Filter.Value == "" {
			log.Warning("NatsUserList.Load: " + UserListFilterValueEmptyWarning)
		}
		if ul.Filter.MaxItems < 1 {
			return errors.New(UserListFilterInvalidMaxItemsError)
		}
		if ul.Filter.Start < 0 {
			return errors.New(UserListFilterInvalidStartIndexError)
		}
		if ul.Filter.SortBy == 0 { // default sort
			ul.Filter.SortBy = AscendingSort
		}

		nc, err := nats.Connect(usersvc.natsUrl, nats.MaxReconnects(messaging.DefaultNatsMaxReconnect), nats.ReconnectWait(messaging.DefaultNatsReconnectWait))
		if err != nil {
			return err
		}
		defer nc.Close()

		ce, err := messaging.CreateCloudEvent(ul, NatsSubjectUsersList, usersvc.clientID)
		if err != nil {
			return err
		}
		payload, err := json.Marshal(ce)

		if err != nil {
			return err
		}

		// retrieve the loaded user from the nats ether
		newctx, cancel := context.WithTimeout(usersvc.ctx, usersvc.requestTimeout)
		msg, err := nc.RequestWithContext(newctx, NatsSubjectUsersList, payload)
		cancel() // no longer need the context, so cancel at this point
		if err != nil {
			return err
		}

		// convert the message to a cloud event
		ce, err = messaging.ConvertNats(msg)
		if err != nil {
			return err
		}

		err = json.Unmarshal(ce.Data(), &ul)
		if err != nil {
			return err
		}

		return nil
	*/
}
