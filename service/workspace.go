package service

import (
	"time"

	"github.com/rs/xid"
	"gitlab.com/cyverse/cacao-common/common"
)

// Workspace contains multiple Deployments. This object does not contain Deployments, but each Deployment object refers the Workspace that it belongs to.
type Workspace struct {
	ID                string    `bson:"_id" json:"id,omitempty"`
	Owner             string    `bson:"owner" json:"owner,omitempty"`
	Name              string    `bson:"name" json:"name,omitempty"`
	DefaultProviderID string    `bson:"default_provider_id" json:"default_provider_id,omitempty"`
	CreatedAt         time.Time `bson:"created_at" json:"created_at,omitempty"`
	UpdatedAt         time.Time `bson:"updated_at" json:"updated_at,omitempty"`
}

// GetCreatedAt returns the creation time of the workspace
func (w Workspace) GetCreatedAt() time.Time {
	return w.CreatedAt
}

// GetCreatedAtString returns the creation time of the workspace
func (w Workspace) GetCreatedAtString() string {
	return w.CreatedAt.String()
}

// GetUpdatedAt returns the modified time of the workspace
func (w Workspace) GetUpdatedAt() time.Time {
	return w.UpdatedAt
}

// GetUpdatedAtString returns the modified time of the workspace
func (w Workspace) GetUpdatedAtString() string {
	return w.UpdatedAt.String()
}

// NewWorkspaceID generates a new WorkspaceID
func NewWorkspaceID() string {
	return "workspace-" + xid.New().String()
}

// WorkspaceListQueryOp is the queue name for WorkspaceList query
const WorkspaceListQueryOp common.QueryOp = common.NatsQueryOpPrefix + "workspace.list"

// WorkspaceList is a request event that is for listing workspaces
type WorkspaceList struct {
	User common.User `json:"user,omitempty"`
}

// WorkspaceListResult is a response event that is for listing workspaces
type WorkspaceListResult common.ServiceRequestResult

// WorkspaceListAdminQueryOp is the queue name for WorkspaceListAdmin query
const WorkspaceListAdminQueryOp common.QueryOp = common.NatsQueryOpPrefix + "workspace.listadmin"

// WorkspaceListAdmin is a request event that is for listing workspaces by admin
type WorkspaceListAdmin WorkspaceList

// WorkspaceListAdminResult is a response event that is for listing workspaces by admin
type WorkspaceListAdminResult WorkspaceListResult

// WorkspaceGetQueryOp is the queue name for WorkspaceGet query
const WorkspaceGetQueryOp common.QueryOp = common.NatsQueryOpPrefix + "workspace.get"

// WorkspaceGet is a request event that is for retrieving a workspace
type WorkspaceGet struct {
	User        common.User `json:"user,omitempty"`
	WorkspaceID string      `json:"workspace_id,omitempty"`
}

// WorkspaceGetResult is a response event that is for retrieving a workspace
type WorkspaceGetResult common.ServiceRequestResult

// WorkspaceGetAdminQueryOp is the queue name for WorkspaceGetAdmin query
const WorkspaceGetAdminQueryOp common.QueryOp = common.NatsQueryOpPrefix + "workspace.getadmin"

// WorkspaceGetAdmin is a request event that is for retrieving a workspace by admin
type WorkspaceGetAdmin struct {
	User        common.User `json:"user,omitempty"`
	WorkspaceID string      `json:"workspace_id,omitempty"`
}

// WorkspaceGetAdminResult is a response event that is for retrieving a workspace by admin
type WorkspaceGetAdminResult WorkspaceGetResult

// WorkspaceCreateRequestedEvent is the cloudevent name of WorkspaceCreate defined below
const WorkspaceCreateRequestedEvent common.EventType = common.EventTypePrefix + "WorkspaceCreateRequested"

// WorkspaceCreate is a request event that is for creating a workspace
type WorkspaceCreate struct {
	User              common.User `json:"user,omitempty"`
	WorkspaceID       string      `json:"workspace_id,omitempty"`
	WorkspaceName     string      `json:"name,omitempty"`
	DefaultProviderID string      `json:"default_provider_id,omitempty"`
}

// WorkspaceCreateAdminRequestedEvent is the cloudevent name of WorkspaceCreateAdmin defined below
const WorkspaceCreateAdminRequestedEvent common.EventType = common.EventTypePrefix + "WorkspaceCreateAdminRequested"

// WorkspaceCreateAdmin is a request event that is for creating a workspace by admin
type WorkspaceCreateAdmin struct {
	User              common.User `json:"user,omitempty"`
	Owner             string      `json:"owner,omitempty"`
	WorkspaceID       string      `json:"workspace_id,omitempty"`
	WorkspaceName     string      `json:"name,omitempty"`
	DefaultProviderID string      `json:"default_provider_id,omitempty"`
}

// WorkspaceCreatedEvent is the cloudevent name of WorkspaceCreated defined below
const WorkspaceCreatedEvent common.EventType = common.EventTypePrefix + "WorkspaceCreated"

// WorkspaceCreated is an event emitted when a Workspace is created
type WorkspaceCreated struct {
	Actor         string `json:"actor,omitempty"`
	Owner         string `json:"owner,omitempty"`
	WorkspaceID   string `json:"workspace_id,omitempty"`
	WorkspaceName string `json:"name,omitempty"`
}

// WorkspaceCreateFailedEvent is the event name of WorkspaceCreateFailed
const WorkspaceCreateFailedEvent common.EventType = common.EventTypePrefix + "WorkspaceCreateFailed"

// WorkspaceCreateFailed is an event emitted when failure occurred during Workspace creation
type WorkspaceCreateFailed struct {
	Actor         string `json:"actor,omitempty"`
	Owner         string `json:"owner,omitempty"`
	WorkspaceID   string `json:"workspace_id,omitempty"`
	WorkspaceName string `json:"name,omitempty"`
	Error         string `json:"error,omitempty"`
}

// WorkspaceDeleteRequestedEvent is the cloudevent name of WorkspaceDelete defined below
const WorkspaceDeleteRequestedEvent common.EventType = common.EventTypePrefix + "WorkspaceDeleteRequested"

// WorkspaceDelete is a request event that is for deleting a workspace
type WorkspaceDelete struct {
	User        common.User `json:"user,omitempty"`
	WorkspaceID string      `json:"workspace_id,omitempty"`
}

// WorkspaceDeleteAdminRequestedEvent is the cloudevent name of WorkspaceDeleteAdmin defined below
const WorkspaceDeleteAdminRequestedEvent common.EventType = common.EventTypePrefix + "WorkspaceDeleteAdminRequested"

// WorkspaceDeleteAdmin is a request event that is for deleting a workspace by admin
type WorkspaceDeleteAdmin struct {
	User        common.User `json:"user,omitempty"`
	WorkspaceID string      `json:"workspace_id,omitempty"`
}

// WorkspaceDeletedEvent is the cloudevent name of WorkspaceDeleted defined below
const WorkspaceDeletedEvent common.EventType = common.EventTypePrefix + "WorkspaceDeleted"

// WorkspaceDeleted is an event emitted when a Workspace is deleted
type WorkspaceDeleted struct {
	Actor         string `json:"actor,omitempty"`
	Owner         string `json:"owner,omitempty"`
	WorkspaceID   string `json:"workspace_id,omitempty"`
	WorkspaceName string `json:"name,omitempty"`
}

// WorkspaceDeleteFailedEvent is the event name of WorkspaceDeleteFailed
const WorkspaceDeleteFailedEvent common.EventType = common.EventTypePrefix + "WorkspaceDeleteFailed"

// WorkspaceDeleteFailed is an event emitted when failure occurred during Workspace deletion
type WorkspaceDeleteFailed struct {
	Actor         string `json:"actor,omitempty"`
	Owner         string `json:"owner,omitempty"`
	WorkspaceID   string `json:"workspace_id,omitempty"`
	WorkspaceName string `json:"name,omitempty"`
	Error         string `json:"error,omitempty"`
}

// WorkspaceUpdateRequestedEvent is the cloudevent name of WorkspaceUpdate defined below
const WorkspaceUpdateRequestedEvent common.EventType = common.EventTypePrefix + "WorkspaceUpdateRequested"

// WorkspaceUpdate is a request event that is for updating a workspace
type WorkspaceUpdate struct {
	User              common.User `json:"user,omitempty"`
	WorkspaceID       string      `json:"workspace_id,omitempty"`
	WorkspaceName     string      `json:"name,omitempty"`
	DefaultProviderID string      `json:"default_provider_id,omitempty"`
}

// WorkspaceUpdateAdminRequestedEvent is the cloudevent name of WorkspaceUpdateAdmin defined below
const WorkspaceUpdateAdminRequestedEvent common.EventType = common.EventTypePrefix + "WorkspaceUpdateAdminRequested"

// WorkspaceUpdate is a request event that is for updating a workspace by admin
type WorkspaceUpdateAdmin struct {
	User              common.User `json:"user,omitempty"`
	Owner             string      `json:"owner,omitempty"`
	WorkspaceID       string      `json:"workspace_id,omitempty"`
	WorkspaceName     string      `json:"name,omitempty"`
	DefaultProviderID string      `json:"default_provider_id,omitempty"`
}

// WorkspaceUpdatedEvent is the cloudevent name of WorkspaceUpdated defined below
const WorkspaceUpdatedEvent common.EventType = common.EventTypePrefix + "WorkspaceUpdated"

// WorkspaceUpdated is an event emitted when a Workspace is updated
type WorkspaceUpdated struct {
	Actor         string `json:"actor,omitempty"`
	Owner         string `json:"owner,omitempty"`
	WorkspaceID   string `json:"workspace_id,omitempty"`
	WorkspaceName string `json:"name,omitempty"`
}

// WorkspaceUpdateFailedEvent is the event name of WorkspaceUpdateFailed
const WorkspaceUpdateFailedEvent common.EventType = common.EventTypePrefix + "WorkspaceUpdateFailed"

// WorkspaceUpdateFailed is an event emitted when failure occurred during Workspace update
type WorkspaceUpdateFailed struct {
	Actor         string `json:"actor,omitempty"`
	Owner         string `json:"owner,omitempty"`
	WorkspaceID   string `json:"workspace_id,omitempty"`
	WorkspaceName string `json:"name,omitempty"`
	Error         string `json:"error,omitempty"`
}
