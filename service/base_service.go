// Package service - this package file will contain base service interfaces and structs for embedding into other service packages
package service

import "gitlab.com/cyverse/cacao-common/common"

// IBaseObject is the base interface for objects for microservices
type SessionContext interface {
	GetSessionActor() string    // username of the person making requests
	GetSessionEmulator() string // set if the Actor's is being emulated by another user, presumably an admin
	SetSessionActor(string)
	SetSessionEmulator(string)
	GetError() (string, string) // gets the error type and message
}

type Session struct {
	SessionActor    string `json:"actor"`
	SessionEmulator string `json:"emulator,omitempty"`
	ErrorType       string `json:"error_type,omitempty"`    // the type of error
	ErrorMessage    string `json:"error_message,omitempty"` // custom or contextual error message
}

func (b *Session) SetSessionActor(s string) {
	b.SessionActor = s
}
func (b *Session) SetSessionEmulator(s string) {
	b.SessionEmulator = s
}
func (b Session) GetSessionActor() string {
	return b.SessionActor
}
func (b Session) GetSessionEmulator() string {
	return b.SessionEmulator
}
func (b Session) GetError() (string, string) {
	return b.ErrorType, b.ErrorMessage
}

// EventSource is currently a placeholder, to be implemented later
type EventObservable interface {
	AddListener(common.EventType, Listener)
	RemoveListener(common.EventType, Listener)
}

// Listener is a currently a placeholder, to be implemented later
type Listener func(ev common.EventType, context interface{})

// EventSource is currently a placeholder
// TODO: implement an asynchronous way to listen to the event queue for eventtypes
// as events of interest are received, then notify
type EventSource struct {
	listeners map[common.EventType][]Listener
}
