package git

import (
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCloneGitHubRepositoryToDisk(t *testing.T) {
	repoConfig := &RepositoryConfig{
		URL:           "https://github.com/cyverse/tf-openstack-single-image",
		Username:      "",
		Password:      "",
		ReferenceType: BranchReferenceType,
		ReferenceName: "master",
	}

	options := &DiskStorageOption{
		ClonePath: "/tmp/test_git",
	}

	clone, err := NewGitCloneToDisk(repoConfig, options)
	assert.NoError(t, err)

	defer clone.Release()

	// test reading a file
	fs := clone.GetFileSystem()
	f, err := fs.Open("metadata.json")
	assert.NoError(t, err)

	data, err := ioutil.ReadAll(f)
	assert.NoError(t, err)
	assert.NotEmpty(t, data)
}

func TestCloneGitHubRepositoryToRAM(t *testing.T) {
	repoConfig := &RepositoryConfig{
		URL:           "https://github.com/cyverse/tf-openstack-single-image",
		Username:      "",
		Password:      "",
		ReferenceType: BranchReferenceType,
		ReferenceName: "master",
	}

	options := &RAMStorageOption{
		SizeLimit: 0,
	}

	clone, err := NewGitCloneToRAM(repoConfig, options)
	assert.NoError(t, err)

	defer clone.Release()

	// test reading a file
	fs := clone.GetFileSystem()
	f, err := fs.Open("metadata.json")
	assert.NoError(t, err)

	data, err := ioutil.ReadAll(f)
	assert.NoError(t, err)
	assert.NotEmpty(t, data)
}

func TestCloneGitLabRepositoryDisk(t *testing.T) {
	repoConfig := &RepositoryConfig{
		URL:           "https://gitlab.com/cyverse/cacao-types",
		Username:      "",
		Password:      "",
		ReferenceType: BranchReferenceType,
		ReferenceName: "master",
	}

	options := &DiskStorageOption{
		ClonePath: "/tmp/test_git",
	}

	clone, err := NewGitCloneToDisk(repoConfig, options)
	assert.NoError(t, err)

	defer clone.Release()

	// test reading a file
	fs := clone.GetFileSystem()
	f, err := fs.Open("README.md")
	assert.NoError(t, err)

	data, err := ioutil.ReadAll(f)
	assert.NoError(t, err)
	assert.NotEmpty(t, data)
}

func TestCloneGitLabRepositoryRAM(t *testing.T) {
	repoConfig := &RepositoryConfig{
		URL:           "https://gitlab.com/cyverse/cacao-types",
		Username:      "",
		Password:      "",
		ReferenceType: BranchReferenceType,
		ReferenceName: "master",
	}

	options := &RAMStorageOption{
		SizeLimit: 0,
	}

	clone, err := NewGitCloneToRAM(repoConfig, options)
	assert.NoError(t, err)

	defer clone.Release()

	// test reading a file
	fs := clone.GetFileSystem()
	f, err := fs.Open("README.md")
	assert.NoError(t, err)

	data, err := ioutil.ReadAll(f)
	assert.NoError(t, err)
	assert.NotEmpty(t, data)
}

func TestCloneGitHubRepositoryToRAMWithSizeLimitFail(t *testing.T) {
	repoConfig := &RepositoryConfig{
		URL:           "https://github.com/cyverse/tf-openstack-single-image",
		Username:      "",
		Password:      "",
		ReferenceType: BranchReferenceType,
		ReferenceName: "master",
	}

	// set 100 bytes size limit, this must fail
	options := &RAMStorageOption{
		SizeLimit: 100, // 100Bytes
	}

	_, err := NewGitCloneToRAM(repoConfig, options)
	// this must fail because it exceeds size limit
	assert.Error(t, err)
}

func TestCloneGitHubRepositoryToRAMWithSizeLimitPass(t *testing.T) {
	repoConfig := &RepositoryConfig{
		URL:           "https://github.com/cyverse/tf-openstack-single-image",
		Username:      "",
		Password:      "",
		ReferenceType: BranchReferenceType,
		ReferenceName: "master",
	}

	// set 1MB size limit, this must succeed
	options := &RAMStorageOption{
		SizeLimit: 1024 * 1024, // 1MB
	}

	clone, err := NewGitCloneToRAM(repoConfig, options)
	// this must pass
	assert.NoError(t, err)

	defer clone.Release()

	// test reading a file
	fs := clone.GetFileSystem()
	f, err := fs.Open("metadata.json")
	assert.NoError(t, err)

	data, err := ioutil.ReadAll(f)
	assert.NoError(t, err)
	assert.NotEmpty(t, data)
}
